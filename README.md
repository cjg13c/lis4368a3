> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 Advanced Web Applications Development

## Connor Griffin

### Assignment 3 Requirements:

*Parts:*

*Deliverables:*
1. Entity Relationship Diagram (ERD)

2. Include data (at least 10 records each table)

3. Provide Bitbucket read-only access to a3 repo (Language SQL), *must* include README.md,using Markdown syntax, and include links to *all* of the following files (from README.md):

    - docs folder: a3.mwb, and a3.sql
	
    - img folder: a3.png (export a3.mwb file as a3.png)
	
    - README.md (*MUST* display a3.png ERD)
	
4. Blackboard Links: a3 Bitbucket repo 
 

#### Assignment Screenshots:

*Screenshot of ERD:

![ERD](img/a3ERD.png)

#### Links:

*Docs folder:*

1. [mwb Link](docs/a3ERD.mwb "MWB Link") 

2. [sql Link](docs/a3ERDsql.sql "SQL Link")
